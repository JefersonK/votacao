# Votacao

Projeto criado com  [Angular CLI](https://github.com/angular/angular-cli) versao 8.0.1. <br/>
Utilizado [Angular material](https://material.angular.io/) versao 8.2.3 <br/>
Todos dados sao FAKE, neste caso, pode ser usado qualdo codigo de funcionario na home.

## Rodar projeto

Execute `ng serve` para iniciar um ambiente de desenvolvimento.  <br/>
Abra no navegador `http://localhost:4200/`.

## Teste unitario

Execute `ng test` para rodar testes unitarios [Karma](https://karma-runner.github.io).

## Todo

* Limitar tempo das votacoes para encerrar ao meio dia;
* Impedir que um restaurante seja escolhido duas vezes na semana
* Melhorar a exibicao dos resultados;
* Implementar o backend (GraphQl) utilizando o framework [Serverless Framework](https://serverless.com/);
* Implementar controle de usuarios (Cognito);

## Obs

Não pude dispor de muito tempo para o desenvolvimento, neste caso o projeto ainda esta incompleto.
